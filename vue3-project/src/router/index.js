// 创建一个路由实例
const router = createRouter({
    // 创建一个WebHashHistory实例
    history: createWebHashHistory(),
    // 路由配置
    routes
})


// 导出router
export default router