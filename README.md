# vue3 + element plus

#### 介绍
vue3最基础入门，vue3 + element plus实战pc端后台管理，从零到一设计pc端项目（提供在线接口请求）

#### 软件架构
电脑端开发的pc项目，项目中以vue3结合最新版的element plus搭建后台管理


steup使用
1.setup函数是 Composition API (组合API)的入口
2.setup是自动执行的一个函数
3.项目中定义的所有变量，方法等都需要在setup中
4.在setup函数中定义的变量和方法最后都需要return 出去，否则无法在视图层中使用

##### 组合API，常用函数：
steup函数
ref函数
reactive函数
toRef函数
toRefs函数
计算属性-computed
watch侦听器
watchEffect
shallowRef和shallowReactive
组件传值
vuex
生命周期图数
vue3的抽离封装


##### vue-cli创建脚手架工程:

1.如果之前安装过Vue2.0的版本，需要把2.0相关的卸载掉
   i npm uninstall vue-cli -g

2.安装最新脚手架
   npm install -g @vue/cli

3.安装成功查看脚手架版本
   vue -V

4.开始构建项目
 (1) vue create '项目名'  回车创建
 如：vue create vue-element-plus
 
 (2)创建时会让你选择配置：
 出现：
 >?please pick a preset:
 > default(babel, eslint)
 > Manually select features

 选择：Manually select features //手动选择

 安装中会出现多次选择，下面只标选择内容：

 (3)自由选择配置(Linter/Formatter插件会检查你的代码变量定义后是否使用)

 *Babel
 Typescript
 Progressive Web App (PWA) Support
 *Router
 *Vuex
 *Css Pre-processors
 *Linter / Formatter
 Unit Testing
 E2E Testing

(4)选择vue3.0版本


(5)选择路由模式:hash模式和history模式
Use history mode for router?
(Requires proper server setup for index fallback in production Y/n)  n

(6)选择css预处理器
Sass/SCSS (with node-sass)

(7)代码类型的检查规范
。ESLint with error prevention only(只检测错误)
。ESLint + Standard config 自带inter和自动代码纠正，没有配置，自动格式化代码，可在编码早起发现规范问题和低级错误
。ESLint + Prettier统一代码风格

选ESLint + Prettier统一代码风格

(8) 选择语法检查方式
*Lint on save (保存就检测)
Lint and fex on commit( 用户提交文件到git的时候检测)

(9) babel,postcss,eslint配置文件存放位置
。 in dedicated config files (在专用配置文件中，就是单独管理)
*。 in packagejson (放在packagejson里）

(10)是否保存为未来项目的预配置  选N


#### 关闭eslint方法
通过vue.config.js配置lintOnSave
module.exports = {
    lintOnSave: false
}

   

#### 安装教程

1.  vue3要安装的插件：
   在vs code先关闭Vetur, 再安装启用Vue Language Features(Volar)。
   提示：这是官网上提示的：推荐使用的 IDE 是 VSCode，配合 Vue 语言特性 (Volar) 插件。该插件提供了语法高亮、TypeScript 支持，以及模板内表达式与组件 props 的智能提示。在https://cn.vuejs.org/guide/scaling-up/tooling.html中（生态系统工具链指南）找到IDE支持。
2.  安装浏览器插件，同上网址，找到浏览器开发者插件，Chrome扩展商店页下载。
    Macbook如何安装vue-devtools?
    1.安装/下载包
      新建一个文件夹,进入.
      打开终端 ,在终端中输入命令(包比较大,又因为在外网条件下,使用下载会慢很多)
        git clone https://github.com/vuejs/vue-devtools

      若遇到下面的情况,不要慌,大概率是网络问题或其他.这里是提示本次操作被远程挂断了,只需重复一次刚刚的下载操作即可:
        remote: Enumerating objects: 11774,done
        error: RPC failed; curl 56 LibressL SsL_read: SSL_ERROR_SYSCALL, errno 54
        fatal: the remote end hung up unexpectedly
        fatal: early EOF
        fatal: index-pack failed
         Air:vuetool安装浏览器扩展 air$

    2.切换到该依赖包下
      cd vue-devtools

    3.切换分支
      注意:这一步操作非常重要,不然会导致后面的扩展程序加载出错
       git status //查看状态 默认 Dev 分支
       git checkout master //手动切换回 master 分支

    4.安装依赖sudo cnpm i
     打开并进入vue-devtools文件夹，在终端中进行操作sudo cnpm i
    
    5.编译
      npm run build

    6.关联浏览器
      打开谷歌浏览器 >> 扩展程序 >> 加载已解压的扩展程序，选择vue-devtools文件夹下的Chrome文件夹.

    7.安装完成

    实践证明：上述下载安装不成功，主要受网络限制，可以这样操作，直接下载vue-devtools压缩包，解压后，打开谷歌浏览器 >> 扩展程序 >> 加载已解压的扩展程序，选择vue-devtools文件夹即可，点击谷歌浏览器的工具栏上的扩展程序，选择固定。
    windows操作系统只要把vue-devtools.crx文件拖到浏览器上就安装了。

   
3.  xxxx

#### 使用说明

1.  vue-project setup
    npm install
2.  Compiles and hot-reloads for development
    npm run serve
3.  Compiles and minifies for production
    npm run bulid

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
